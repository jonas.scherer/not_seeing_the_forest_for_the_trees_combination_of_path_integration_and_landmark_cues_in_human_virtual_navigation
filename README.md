# Not seeing the forest for the trees: Combination of path integration and landmark cues in human virtual navigation
Jonas Scherer, Martin M. Müller, Patrick Unterbrink, Sina Meier, Martin Egelhaaf, Olivier J. N. Bertrand, Norbert Boeddeker

## readme

This repository accompanies our publication in [frontiers in Behavioral Neuroscience](https://www.frontiersin.org/journals/behavioral-neuroscience/articles/10.3389/fnbeh.2024.1399716/full).

It includes jupyter notebooks to comprehend in depth the analysis steps explained in the publication.

Data is available separately from [here](https://pub.uni-bielefeld.de/record/2982046).

Instructions how to clone the repository, download the data, and run the notebooks is given below. 

## setup

To open and execute the jupyter notebooks in this repository, follow the steps below:

- set up conda environment
    - Clone the repository
    - Open a conda console (on windows you can add conda to your git bash console like explained [here](https://discuss.codecademy.com/t/setting-up-conda-in-git-bash/534473) (last access 24.08.2023))
    - Create a new conda environment with ```conda create -n not_seeing_the_forest python=3.9.12``` (you can name it as you like, but this text will assume it to be called 'not_seeing_the_forest')
    - Activate the environment with ```conda activate not_seeing_the_forest```

- install necessary python packages
    - activate your conda environment
    - install all necessary python packages in the respective version from the requirements.txt file with ```conda install -c conda-forge --file requirements.txt```

- install R packages
    - activate your conda environment
    - start R with ```R```
    - install the required R packages with ```install.packages(c("moments", "lme4", "ggplot2", "ggpubr", "lmtest", "jtools"))```

- download the data
    - go [here](https://pub.uni-bielefeld.de/record/2982046) and download the study's data as a zip archive
    - unzip the "data" folder in the cloned repository
    - make sure that the "data" folder is on the same folder level as the "code" folder 

- start jupyter notebook
    - inside your git bash navigate to the cloned repository and start jupyter notebook with ```jupyter notebook```
    - open the analysis notebooks by clicking on them
    - you can run cells with ctrl + Enter, or run all cells in a notebook by clicking on Cell --> Run All


## analysis notebooks summary

The provided jupyter notebooks in "code" include all analyses described in our paper "Not seeing the forest for the trees: Combination of path integration and landmark cues in virtual navigation".
Here are short summaries of the notebooks:

- nb_00_process_raw_data: 
    - reads in raw data
    - cleans NaN values
    - throws out participants as described in paper
    - unifies session counts for participants
    - calculates error measures, calculates parameters for MLE modelling

- nb_01_trajectory_plotting: 
    - plots single participant's and pooled trajectories and endpoints
    - plots Figure 1
    - plots Figure 2
    - plots Figure 3
    - plots Figure S1
    - plots Figure 7a-f

- nb_02_position_error_plotting:
    - plots position error (accuracy) and position error standard deviation (precision) as boxplots
    - separated by condition (number of objects)
    - single data points are median measures per participant
    - plots quadratic fits for accuracy and precision in logarithmic space
    - plots Figure 4
    - plots Figure 5

- nb_03a_linear_regression_modelling_accuracy:
    - guides through the linear mixed effect modelling we conducted for position error
    - motivates and justifies analysis steps
    - calculates confidence intervals for the quadratic fits in nb_02 ("quadratic_fit_CIs")
    - calculates position error differences between zero and three objects, and between three and 99 objects for nb_04 ("diffs")
    - calculates differences also separately for session 1 and session 4 ("diffs_only_session1", "diffs_only_session4")

- nb_03a2_regression_models_on_war_single_trials_accuracy:
    - presents alternative linear regression analysis based on single trials rather than median position error

- nb_03p_linear_regression_modelling_precision:
    - same as notebook nb_03a_linear_regression_modelling_accuracy but for position error standard deviation (precision)
    - calculates "sd_quadratic_fit_CIs" for nb_02
    - calculates "sd_diffs" for nb_04
    - calculates differences also separately for session 1 and session 4 ("sd_diffs_only_session1", "sd_diffs_only_session4")

- nb_04_condition_and_individuality_median_differences:
    - plots differences between zero and three object condition, and between three and 99 object condition from "diffs" and     "sd_diffs"
    - plots Figure 6
    - provides statistical analysis for kernel density distributions in Figure 6

- nb_05_precision_accuracy_regression:
    - calculates simple linear regressions to predict accuracy (position error) from precision (position error std) for each condition
    - plots Figure 7

- nb_06_mle_heatmaps:
    - plots MLE overview panels
    - fits MLE models to the empirical data
    - plots Figure 8a-h

- nb_07_mle_bootstrap:
    - bootstraps MLE model parameters
    - plots Figure 9

- nb_08_order_effect:
    - plots accuracy (position error) and precision (position error std) over the different sessions
    - plots Figure S2
