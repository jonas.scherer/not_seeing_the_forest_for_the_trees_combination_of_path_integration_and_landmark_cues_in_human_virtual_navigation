"""
:AUTHOR: Jonas Scherer
:ORGANIZATION: Department of Neurobiology and Department of Cognitive Neuroscience, Bielefeld University, Germany
:CONTACT: jonas.scherer@uni-bielefeld.de
:SINCE: Mon Dec 12 11:08:00 2022
:VERSION: 0.1

This module contains data cleaning functions.
"""

import numpy as np
import pandas as pd
from scipy import stats
from utilities import helpers


def flag_training(df, flag_name="flagged", train_name="IsTraining"):
    """
    Flag all rows which are marked as training in extra column.
    (Currently simply duplicates the "IsTraining" column in a new column)

    Parameters
    ----------
    df : pandas dataframe
        the dataframe to be checked.
    flag_name : string, optional
        the column to be created for the flag boolean.
        The default is "flagged".
    train_name : string, optional
        the column name marking trials as training.
        The default is "IsTraining".

    Returns
    -------
    df : pandas dataframe
        the modified dataframe.

    """
    to_flag = df[train_name]
    print(f"   ==> flagged {sum(to_flag)} rows for removal as training.")
    # create flag column if it does not exist yet
    if flag_name not in df:
        df.loc[:, flag_name] = to_flag
    # if it does exist: modify it
    else:
        df.loc[:, flag_name] = np.logical_or(df[flag_name], to_flag)
    return df


# from: https://stackoverflow.com/questions/23199796/detect-and-exclude-outliers-in-pandas-data-frame
# median-based alternative: https://scipy.github.io/devdocs/generated/scipy.stats.median_abs_deviation.html#scipy.stats.median_abs_deviation
def flag_outliers(
    df,
    colname,
    spatial=False,
    z_score_thresh=3,
    spatial_iqr_thresh=3,
    flag_name="flagged",
):
    """
    Flag all rows which exceed a z-score threshold or a spatial IQR threshold for coordinates in extra column.

    Parameters
    ----------
    df : pandas dataframe
        the dataframe to be checked.
    colname : string
        the name of the column to be checked.
    spatial : boolean, optional
        allows calculating outliers based on spatial centroid.
        If true colname needs to be a list or array-like providing indices
        to 2 or 3 columns that contain numerical coordinates.
        Outliers are defined as points whose distance from their respective centroids
        exceed the 3rd quartile by <spatial_iqr_thresh> times the interquartile range
    z_score_thresh : float, optional
        the threshold for the outlier detection. The default is 3.
    spatial_iqr_thresh : float, optional
        only used when spatial==True, Outliers are defined as points whose distance from
        their respective centroids exceed the 3rd quartile by <spatial_iqr_thresh> times
        the interquartile range
    flag_name : string, optional
        the column to be created for the flag boolean.
        The default is "flagged".

    Returns
    -------
    df : pandas dataframe
        the modified dataframe.

    """
    if not spatial:
        # we calculate the z-scores for the column and then flag any which exceed
        # our threshold
        to_flag = np.abs(stats.zscore(df[colname])) > z_score_thresh
        print(
            f"   ==> flagged {sum(to_flag)} rows for removal as outlier"
            + f" in column {colname}."
        )
    else:
        # check that colname is list  or array
        if not (type(colname) == list) | (type(colname) == np.ndarray):
            msg = f"colname needs to be list or numpy array but it is {type(colname)}"
            raise ValueError(msg)
        else:
            # check that colname has 2 or 3 entries
            if not 2 <= len(colname) <= 3:
                msg = f"colname needs to contain 2 or three columns but it contains {len(colname)}"
                raise ValueError(msg)
            else:
                # get coordinates of endpoints and calculate centroid
                coordinates = np.array(df[colname])
                centroid = np.sum(coordinates, axis=0) / len(coordinates)
                dists = np.linalg.norm(coordinates - centroid, axis=1)
                thresh = np.percentile(dists, 75) + spatial_iqr_thresh * stats.iqr(
                    dists
                )  # 3 times iqr from 3rd quartile
                to_flag = dists > thresh
                print(
                    f"   ==> flagged {sum(to_flag)} rows for removal as spatial outliers"
                    + f" in column {colname}."
                )
    # create flag column if it does not exist yet
    if flag_name not in df:
        df.loc[:, flag_name] = to_flag
    # if it does exist: modify it
    else:
        df.loc[:, flag_name] = np.logical_or(df[flag_name], to_flag)
    return df


def flag_missing_data(df, colname, flag_name="flagged"):
    """
    Flag all rows which have missing data in extra column.

    Parameters
    ----------
    df : pandas dataframe
        the dataframe to be checked.
    colname : string
        the name of the column to be checked.
    flag_name : string, optional
        the column to be created for the flag boolean.
        The default is "flagged".

    Returns
    -------
    df : pandas dataframe
        the modified dataframe.

    """
    to_flag = df[colname].isnull()
    if flag_name not in df:
        df.loc[:, flag_name] = to_flag
    else:
        df.loc[:, flag_name] = np.logical_or(df[flag_name], to_flag)
    print(
        f"   ==> flagged {sum(to_flag)} rows for removal"
        + f" because of missing data in {colname}."
    )
    return df


def flag_moving_not_possible_in_homing(df, params, flag_name="flagged"):
    """
    Flag all rows where participants were not able to move the avatar when homing started.
    This error is detected based on the angular error being precisely == -90 or == +90

    Parameters
    ----------
    df : pandas dataframe
        the dataframe to be checked.
    flag_name : string, optional
        the column to be created for the flag boolean.
        The default is "flagged".

    Returns
    -------
    df : pandas dataframe
        the modified dataframe.

    """
    to_flag = df[params[0]] == params[1]
    if flag_name not in df:
        df.loc[:, flag_name] = to_flag
    else:
        df.loc[:, flag_name] = np.logical_or(df[flag_name], to_flag)
    print(
        f"   ==> flagged {sum(to_flag)} rows for removal"
        + f" because there was no movment in homing based on column {params[0]}."
    )
    return df


def flag_no_homing(df, flag_name="flagged", circle_thresh_in_m=2):
    """
    Flag all rows where there is no trajectory labeled "homing" or "Homing" in column "trial_state"

    Parameters
    ----------
    df : pandas dataframe
        the dataframe to be checked.
    flag_name : string, optional
        the column to be created for the flag boolean.
        The default is "flagged".
    circle_thresh_in_m : int
        distance that at least needs to be covered by participants to be counted as valid homing
        defaults to 2

    Returns
    -------
    df : pandas dataframe
        the modified dataframe.

    """
    DF = df.copy()
    try:
        DF = df.reset_index()
    except:
        print("df index already reset")
    try:
        DF = DF.set_index(["session_num", "block_num", "trial_num"])
    except:
        print("trial_num already in df index")
    DF = DF.loc[["train" not in a for a in DF.trial_name]]
    idxs = np.unique(DF.index)
    flagged = np.array([])
    to_flag = np.array([], dtype=bool)
    n_flagged_trials = 0
    for i, idx in enumerate(idxs):
        df = DF.loc[idx]
        homing = df.loc[(df.trial_state == "Homing") | (df.trial_state == "homing")]
        if (len(homing) == 0):
            to_flag = np.append(to_flag, np.repeat(True, len(df)))
            n_flagged_trials += 1
            flagged = np.append(flagged, idx)
            print(idx)
        else:
            # check if player moved across a certain threshold
            crow_fly = np.linalg.norm([homing.loc[:, "pos_x"] - homing.iloc[0].pos_x,
                                     homing.loc[:, "pos_z"] - homing.iloc[0].pos_z], axis=0)
            moved_enough = np.max(crow_fly) > circle_thresh_in_m # player must at least move ... meters
            if not moved_enough:
                to_flag = np.append(to_flag, np.repeat(True, len(df)))
                n_flagged_trials += 1
                flagged = np.append(flagged, idx)
                print(idx)
            else:
                to_flag = np.append(to_flag, np.repeat(False, len(df)))
    if flag_name not in df:
        DF.loc[:, flag_name] = to_flag
    else:
        DF.loc[:, flag_name] = np.logical_or(DF[flag_name], to_flag)
    print(
        f"   ==> flagged {sum(to_flag)} rows ({n_flagged_trials} trials) for removal"
        + f" because there is no trial state <<Homing>>, or player did not move more than {circle_thresh_in_m} meters."
    )
    DF = DF.reset_index()
    DF = DF.set_index(["session_num", "block_num", "trial_name", "line_i"])

    return DF


def flag_wrong_rotation_direction(df, timeframe_in_s=2, flag_name="flagged", turn_thresh_in_deg=0.5, walk_thresh_in_m=0.2, circle_thresh_in_m=2):
    """
    Flags rows in indicated column if the rotation direction of the moving player does not correspond
    to the inteded turning direction indicated by the trial name

    Parameters
    ----------
    df : pandas DataFrame
        DataFrame of the player movement.
    timeframe : int
        timeframe in seconds from the beginning of the homing that is considered. This is necessary as
        people might do the right rotation in the beginning, but than do random circles later in the trial.
    flag_name : str, optional
        name of the column that the boolean flags for right or wrong rotation directions are stored in
        The default is "flagged".
    turn_thresh : numeric, optional
        amount of angle turned before trajectory is respected for calculations. Whatever happens first; rotation oe translation
    walk_thresh : numeric, optional
        amount of distance walked before trajectory is respected for calculation. Whatever happens first; rotation oe translation
    circle_thresh_in_m : int
        distance that at least needs to be covered by participants to be counted as valid homing
        defaults to 2
    
    Returns
    -------
    DF : pandas DataFrame
        The same dataframe as input with additional or altered flag column.

    """
    DF = df.copy()
    try:
        DF = df.reset_index()
    except:
        print("df index already reset")
    try:
        DF = DF.set_index(["session_num", "block_num", "trial_num"])
    except:
        print("trial_num already in df index")
    DF = DF.loc[["train" not in a for a in DF.trial_name]]
    idxs = np.unique(DF.index)
    flagged = np.array([])
    to_flag = np.array([], dtype=bool)
    n_flagged_trials = 0
    for i, idx in enumerate(idxs):
        df = DF.loc[idx]
        homing = df.loc[df.trial_state == "Homing"]
        if len(homing) == 0:
            to_flag = np.append(to_flag, np.repeat(False, len(df)))
            continue
        homing_rot = list(homing.eul_y)
        homing_rot = np.unwrap(
            homing_rot, period=360
        )  # making homing rotation direction continuous
        # cumsum turned angle to see where thresh is achived
        turned = homing_rot
        turned = turned - turned[0]
        turned = np.cumsum(turned)
        # index where angle threshold is achived
        try:
            turn_thresh_idx = np.argwhere(abs(turned) > turn_thresh_in_deg)[0][0]
        except:
            turn_thresh_idx = len(turned) - 1 
        # cumsum walked distance to see where thresh is achived
        walked = np.linalg.norm([np.diff(homing.loc[:, "pos_x"]), np.diff(homing.loc[:, "pos_z"])], axis=0)
        walked = np.cumsum(walked)
        # index where dist threshold is achived
        try:
            walk_thresh_idx = np.argwhere(abs(walked) > walk_thresh_in_m)[0][0]            
        except: # catch trials where player does not reach walk threshold
            walk_thresh_idx = len(homing) - 1
        # set movement_start to whatever occurrs earlier; translation or rotation
        movement_start_idx = np.min([turn_thresh_idx, walk_thresh_idx])
        # in some trials participants walked a curved path for the second triangle side
        # ... and did not rotate at all in homing
        # so if the variance is lower than the set threshold we do not flag either
        crow_fly = np.linalg.norm([homing.loc[:, "pos_x"] - homing.iloc[0].pos_x,
                                 homing.loc[:, "pos_z"] - homing.iloc[0].pos_z], axis=0)
        try:
            circle_thresh_idx = np.argwhere(crow_fly > circle_thresh_in_m)[0][0]
        except: # catch trials where player does not reach circle threshold
            circle_thresh_idx = len(homing) - 1
        timestamps = list(df.timestamp)
        framerate = helpers.get_framerate(timestamps)
        # calculate number of samples within this timeframe
        time_from_start = int(np.round(framerate) * timeframe_in_s) + movement_start_idx
        stop_idx = np.min([circle_thresh_idx, time_from_start])
        stop_idx = circle_thresh_idx
        
        if np.var(homing.iloc[movement_start_idx:stop_idx].eul_y) <= 2:  # only if participant rotated in trial
            movement_start_idx = 0
     
        looked_correctly = 0
        homing_of_interest = homing_rot[movement_start_idx : stop_idx]
        if any((homing_of_interest > 160)) & any((homing_of_interest < 200)):
            looked_correctly = 1
        
        turn_dir = np.diff(homing_of_interest)
        turn_sum = np.sum(turn_dir)
        # get intended turning direction
        curr_trial_name = df.loc[idx].iloc[0].trial_name
        if (
            ("right" in curr_trial_name)
            | ("Right" in curr_trial_name)
            | ("RIGHT" in curr_trial_name)
        ):
            if (turn_sum < 0) & (not looked_correctly):
                to_flag = np.append(to_flag, np.repeat(True, len(df)))
                n_flagged_trials += 1
                flagged = np.append(flagged, idx)
                print(idx)
            else:
                to_flag = np.append(to_flag, np.repeat(False, len(df)))
        elif (
            ("left" in curr_trial_name)
            | ("Left" in curr_trial_name)
            | ("LEFT" in curr_trial_name)
        ):
            if (turn_sum > 0) & (not looked_correctly):
                to_flag = np.append(to_flag, np.repeat(True, len(df)))
                n_flagged_trials += 1
                flagged = np.append(flagged, idx)
            else:
                to_flag = np.append(to_flag, np.repeat(False, len(df)))
        else:
            msg = "neither <right> nor <left> in trial_name -> can't infer desired rotation direction"
            raise ValueError(msg)
    if flag_name not in df:
        DF.loc[:, flag_name] = to_flag
    else:
        DF.loc[:, flag_name] = np.logical_or(DF[flag_name], to_flag)
    print(
        f"   ==> flagged {sum(to_flag)} rows ({n_flagged_trials} trials) for removal"
        + f" because rotation direction was not as desired by trial_name."
    )
    DF = DF.reset_index()
    DF = DF.set_index(["session_num", "block_num", "trial_name", "line_i"])
    return DF


def clean_dataset(
    dataset,
    variant="session",
    remove_flagged=True,
    check_outliers=True,
    check_no_homing=True,
    check_wrong_rotation_direction=False,
    reset=None,
):
    """
    Check dataset for anomalies and remove them if desired.
    This function directly modifies the dataframe provided to it. 
    Note that "check_wrong_rotation_direction" defaults to False, meaning corresponding trials are kept in the analysis.

    Parameters
    ----------
    dataset : pandas dfmi
        the dataset from a single participant contained in a pandas dataframe.
    variant : "session" or "trajectory"
        whether given dataset is session or trajectory data, funciton checks different columns for outliers then
    remove_flagged : boolean, optional
        flag to decide whether anomalies should be only flagged
        or removed directly. The default is True.
    check_outliers : boolean, optional
        whether or not to check for outliers
    check_wrong_rotation_direction : boolean, optional
        whether or not to check if the rotation direction fits the dataframes trial_name
    wrong_rotation_direction_timeframe : numeric
        time in seconds used at start of homing to infer the turning direction
    reset : string
        string name of column to be reset, this column is then dropped from the dataframe
    
    Returns
    -------
    dataset : pandas dataframe
        the modified input dataset.

    """
    if (type(reset) == str) & (reset in dataset.columns):
        print(f"resetting column {reset}")
        dataset.drop(reset, axis=1, inplace=True)
    if variant == "session":
        "flag anomalies"
        # flag training
        dataset = flag_training(dataset)
        if check_outliers:
            # flag outliers
            cols_to_check_for_outliers = ["goalEstimateX", "goalEstimateZ"]
            for col in cols_to_check_for_outliers:
                dataset = flag_outliers(dataset, col)
        # flag missing walking data
        to_check = [
            "goalEstimateX",
            "goalEstimateY",
            "goalEstimateZ",
            "goalTrueX",
            "goalTrueY",
            "goalTrueZ",
            "confidence",
        ]
        for col in to_check:
            dataset = flag_missing_data(dataset, col)
    elif variant == "trajectory":
        if check_outliers:
            # flag outliers
            cols_to_check_for_outliers = ["pos_x", "pos_z"]
            for col in cols_to_check_for_outliers:
                dataset = flag_outliers(dataset, col)
        # flag missing walking data
        to_check = ["pos_x", "pos_z"]
        for col in to_check:
            dataset = flag_missing_data(dataset, col)
        if check_no_homing:
            dataset = flag_no_homing(dataset)
        if check_wrong_rotation_direction:
            dataset = flag_wrong_rotation_direction(dataset)
    # sum up flagging results
    n_flagged = dataset["flagged"].sum()
    print(f"flagged a total of {n_flagged} out of {len(dataset.index)} rows")

    if remove_flagged:
        dataset = dataset[~dataset["flagged"]]
        dataset = dataset.drop(labels=["flagged"], axis="columns")
        print("removed all flagged rows!")
    return dataset
