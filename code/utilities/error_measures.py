"""
:AUTHOR: Jonas Scherer
:ORGANIZATION: Department of Neurobiology and Department of Cognitive Neuroscience, Bielefeld University, Germany
:CONTACT: jonas.scherer@uni-bielefeld.de
:SINCE: Mon Dec 12 11:08:00 2022
:VERSION: 0.1

This module contains functions to calculate error measures.
"""

import numpy as np
from scipy import linalg

def calculate_vectors(goalPos, startPos, actualPos):
    """
    calculates vectors from the start of the homing trip to the actual goal
    and to the observed goal estimates.

    Parameters
    ----------
    goalPos : (N, 2) array-like
        list [x, y] coordinates for the goal.
    startPos : (N, 2) array-like
        list [x, y] coordinates for the homing start position.
    actualPos : (N, 2) array-like
        list [x, y] coordinates for the goal estimate.

    Returns
    -------
    observedVect : (N, 2) numpy array
    array of vectors [x, y] from homing start to goal estimate.
    targetVect : (N, 2) numpy array
        array of vectors [x, y] from homing start to actual goal.
    """
    # cast input to numpy array
    goalPos = np.asarray(goalPos)
    startPos = np.asarray(startPos)
    actualPos = np.asarray(actualPos)
    # create vector [x, y] from home start to goal
    targetVect = np.subtract(goalPos, startPos)
    # create vector [x, y] from home start to home estimate
    observedVect = np.subtract(actualPos, startPos)

    return observedVect, targetVect

def calc_position_error(observed_vector, target_vector):
    """
    Calculates the difference between the correct and observed position as a vector and its length. I.e. for
    observed vector o and target vector t it calculates: position_error = o - t.
    The calculation expects the Homing Start to be the origin of the coordinate system, i.e. [0,0].

    Parameters
    ----------
    observed_vector : (N, 2) numpy array
        array of vectors [x, y] containing the vector from homing start to observed goal estimate.
    target_vector : (N, 2) numpy array
        array of vectors [x, y] containing the vector from homing start to target goal.

    Returns
    -------
    position_error_norm : (N, 1) numpy array
        difference between observed and correct position as vector length.
    position_error : (N, 2) numpy array
        difference between observed and correct position as vector.
    """

    position_error = np.subtract(observed_vector, target_vector)
    try:
        position_error_norm = linalg.norm(position_error, axis=1)
    except:
        position_error_norm = linalg.norm(position_error, axis=0)
    return position_error_norm, position_error

def calc_distance_error(observed_vector, target_vector):
    """
     Calculates the difference between target and observed distances. I.e. for observed vector o and target vector t
     the distance error is calculated as follows: dist_error = norm(o) - norm(t)
     If the target distance is overshot, the distance error will be positive.
     The calculation expects the Homing Start to be the origin of the coordinate system, i.e. [0,0].

    Parameters
    ----------
    observed_vector : (N, 2) numpy array
        array of vectors [x, y] containing the vector from homing start to observed goal estimate.
    target_vector : (N, 2) numpy array
        array of vectors [x, y] containing the vector from homing start to target goal.

    Returns
    -------
    dist_error : (N, 1) numpy array
        the difference between target and observed distances.
    """
    try:
        norm_observed = linalg.norm(observed_vector, axis=1)
        norm_target = linalg.norm(target_vector, axis=1)
    except:
        norm_observed = linalg.norm(observed_vector, axis=0)
        norm_target = linalg.norm(target_vector, axis=0)
    dist_error = np.subtract(norm_observed, norm_target)

    return dist_error


def calc_directional_error(
    observed_vector, target_vector, leftIsNegative=True, convertToDegrees=False
):
    """
    Calculates the difference between the target and observed turning angles, i.e. the error in direction in radian.
    By default, negative errors indicate a leftwards deviation from the target direction.
    The calculation expects the Homing Start to be the origin of the coordinate system, i.e. [0,0].

    Parameters
    ----------
    observed_vector : (N, 2) numpy array
        array of vectors [x, y] containing the vector from homing start to observed goal estimate.
    target_vector : (N, 2) numpy array
        array of vectors [x, y] containing the vector from homing start to target goal.
    leftIsNegative: bool
        optional, set to False to reverse the directional convention. The default is True.

    Returns
    -------
    angles : (N, 1) numpy array
        the difference between target and observed direction.
    """

    # calculate angles
    angles = []
    for observed, target in zip(observed_vector, target_vector):
        angles.append(calculate_angle(observed, target, convertToDegrees))
    if not leftIsNegative:
        angles = np.dot(angles, -1)  # flip l/r
    return angles

def calculate_angle(v1, v2, convertToDegrees=False):
    """
    calculates the angle between the given 2D-vectors.

    Parameters
    ----------
    v1 : (1, 2) array-like
        a pair of [x, y] coordinates.
    v2 : (1, 2) array-like
        a pair of [x, y] coordinates.
    convertToDegrees : bool, optional
        whether to convert result from radians to degrees.
        The default is False.

    Returns
    -------
    angle : scalar
        the angle between the two given vectors
        in the interval [-180, +180] degrees, or [-pi, +pi] radians.
    """

    v1 = tuple(v1)
    v2 = tuple(v2)
    # calculate angle
    angle = np.arctan2(v2[1], v2[0]) - np.arctan2(v1[1], v1[0])
    # handle quadrants
    if angle > np.pi:
        angle -= 2 * np.pi
    elif angle <= -np.pi:
        angle += 2 * np.pi
    # determine output format
    if convertToDegrees:
        angle = np.degrees(angle)
    return angle