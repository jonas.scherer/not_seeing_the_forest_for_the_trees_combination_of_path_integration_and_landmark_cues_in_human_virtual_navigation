"""
:AUTHORS: Martin M. Müller
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: martin.mueller@uni-bielefeld.de
:SINCE: Fri Dec 10 11:04:58 2021
"""
import numpy as np
from scipy.stats import chi2


def calc_aic(log_likelihood, n_params, sample_size):
    """
    Calculate the Akaike information criterion for the given model likelihood,
    parameters and sample size.

    Parameters
    ----------
    log_likelihood : scalar
        the likelihood of the model for which to calculate the AIC.
    n_params : scalar
        number of free parameters of the model.
    sample_size : scalar
        number of samples used to create the likelihood.

    Returns
    -------
    aic : scalar
        the Akaike information criterion of the model.

    """
    aic = 2 * n_params - 2 * log_likelihood
    # apply correction for small samples if required
    if sample_size <= 100 or n_params > 5:
        aic = aic + (2 * n_params * (n_params + 1)) / (
            sample_size - n_params - 1
        )
        print(
            "sample size was < 100 or n_params was > 5, using bias-corrected AIC variant."
        )
    return aic


def calc_bic(log_likelihood, n_params, sample_size):
    """
    Calculate the Bayesian (also known as Schwarz) information criterion
    for the given model likelihood, parameters and sample size.

    Parameters
    ----------
    log_likelihood : scalar
        the likelihood of the model for which to calculate the BIC.
    n_params : scalar
        number of free parameters of the model.
    sample_size : scalar
        number of samples used to create the likelihood.

    Returns
    -------
    bic : scalar
        the Bayesian information criterion of the model.

    """
    bic = n_params * np.log(sample_size) - 2 * log_likelihood
    return bic


def likelihood_ratio_test(ll_base, ll_constrained, n_params_diff):
    """
    Calculate the likelihood ratio for two nested models and return a p-value
    for the chi-squared test statistic.

    For non-nested models, use AIC or BIC instead!

    Parameters
    ----------
    ll_base : scalar
        log-likelihood of the model with more degrees of freedom.
    ll_constrained : scalar
        log-likelihood of the model with fewer degrees of freedom.
    n_params_diff : scalar
        difference between degrees of freedom for the two models (must be >=1 !).

    Raises
    ------
    ValueError
        catches invalid values for difference in degrees of freedom.

    Returns
    -------
    likelihood_ratio : scalar
        the likelihood ratio of the two models to be compared.
    p_val : scalar
        the p value of the chi-square statistic.

    """
    likelihood_ratio = -2 * (ll_constrained - ll_base)
    if n_params_diff <= 0:
        raise ValueError(
            "difference in degrees of freedom must be a positive integer!\n"
            + "if both models have same degrees of freedom,"
            + " compare likelihoods directly instead of using this test."
        )
    """
    evaluate chi-squared survival (1-cdf) function
    at x=likelihood_ratio to get p-value
    """
    p_val = chi2.sf(likelihood_ratio, n_params_diff)
    return likelihood_ratio, p_val
