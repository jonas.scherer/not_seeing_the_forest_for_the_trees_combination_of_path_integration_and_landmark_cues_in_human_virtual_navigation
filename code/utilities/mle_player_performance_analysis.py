"""
:AUTHORS: Martin M. Müller
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: martin.mueller@uni-bielefeld.de
:SINCE: Wed Dec 15 14:17:14 2021
"""

import numpy as np
import pandas as pd

from utilities import error_measures as measures


def analyse_dataset(df, convertToDegrees=True):
    """
    Analyse dataset by computing angle, distance and position errors.
    This function directly modifies the dataframe provided to it.

    Parameters
    ----------
    df : pandas dfmi
        the dataset from a single participant contained in a pandas dataframe.
    convertToDegrees : boolean, optional
        a flag to determine whether angle error should be in degrees or radian.
        The default is True.

    Returns
    -------
    df : pandas dfmi
        the analysed dataset.

    """
    goal = pd.concat([df["goalTrueX"], df["goalTrueZ"]], axis=1)
    start = pd.concat([df["startPosX"], df["startPosZ"]], axis=1)
    walked = pd.concat([df["goalEstimateX"], df["goalEstimateZ"]], axis=1)

    observedVect, targetVect = measures.calculate_vectors(goal, start, walked)

    dist_err_walk = measures.calc_distance_error(
        np.array(observedVect), np.array(targetVect)
    )
    dir_err_walk = measures.calc_directional_error(
        np.array(observedVect), np.array(targetVect)
    )
    pos_err_walk, _ = measures.calc_position_error(
        np.array(observedVect), np.array(targetVect)
    )

    if convertToDegrees:
        dir_err_walk = np.degrees(dir_err_walk)
    df.loc[:, "err_dist_walk"] = dist_err_walk
    df.loc[:, "err_dir_walk"] = dir_err_walk
    df.loc[:, "err_pos_walk"] = pos_err_walk

    return df


def calc_error_measures_for_LM(
    df, obj_pos, name_suffix, convertToDegrees=True
):
    """
    Calculate distance, angle and position error relative to a given location
    in the environment (usually a landmark object)

    Parameters
    ----------
    df : pandas dataframe
        the dataset from a single participant contained in a pandas dataframe.
    obj_pos : (1,2) array-like
        the position on which to centre the error measures.
    name_suffix : string
        the suffix used for output column names
        (pattern is: "measure_name_" + name_string).
    convertToDegrees : boolean, optional
        a flag to determine whether angle error should be in degrees or radian.
        The default is True.

    Returns
    -------
    df : pandas dataframe
        the input dataframe with added columns for the calculated measures

    """
    goal = pd.concat([df["goalTrueX"], df["goalTrueZ"]], axis=1)
    start = pd.DataFrame(data=np.tile(obj_pos, (len(goal), 1)), dtype="float")
    walked = pd.concat([df["goalEstimateX"], df["goalEstimateZ"]], axis=1)

    observedVect, targetVect = measures.calculate_vectors(goal, start, walked)
    dist_err_LM = measures.calc_distance_error(
        np.array(observedVect), np.array(targetVect)
    )
    dir_err_LM = measures.calc_directional_error(
        np.array(observedVect), np.array(targetVect)
    )
    pos_err_LM, _ = measures.calc_position_error(
        np.array(observedVect), np.array(targetVect)
    )

    if convertToDegrees:
        dir_err_LM = np.degrees(dir_err_LM)
    df.loc[:, "err_dist_" + name_suffix] = dist_err_LM
    df.loc[:, "err_dir_" + name_suffix] = dir_err_LM
    df.loc[:, "err_pos_" + name_suffix] = pos_err_LM

    return df
