"""
:AUTHORS: Martin M. Müller
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: martin.mueller@uni-bielefeld.de
:SINCE: Tue Nov 23 12:23:57 2021
"""
import numpy as np
from scipy.stats import vonmises
import scipy.integrate as integrate
from scipy.stats import norm
from scipy.stats import gamma
from scipy.stats import uniform


def calc_uniform_probability_density(x, lb, width, validate_pdf=False):
    """
    Calculate probability densities for given set of points x
    from a uniform probability density function.

    (this function is a wrapper for scipy.stats.uniform.pdf())

    Parameters
    ----------
    x : (N,1) array-like
        the points for which to calculate probability densities.
    lb : scalar
        lower bound (e.g. 0 or -np.inf).
    width : scalar
        how wide to make the range of the function, starting at lb.

    validate_pdf : bool, optional
        whether to check that the underlying function is a pdf. The default is False.

    Returns
    -------
    p_uniform : (N,1) array-like
        probability values for x.

    """
    p_uniform = uniform.pdf(x, lb, width)
    if validate_pdf == True:
        min_x = lb
        max_x = lb + width
        integral, _ = integrate.quad(lambda x: uniform.pdf(x=x), min_x, max_x)
        print("validation that uniform func used is pdf:")
        print(
            f" => integral over the interval [{min_x},{max_x}] was: {integral}"
        )
    return p_uniform


def calc_gaussian_probability_density(x, mu, sigma, validate_pdf=False):
    """
    Calculate probability densities for given set of points x
    from a gaussian probability density function
    with mean mu and standard deviation sigma.

    (this function is a wrapper for scipy.stats.norm.pdf())

    Parameters
    ----------
    x : (N,1) array-like
        the points for which to calculate probability densities.
    mu : scalar
        expected value of the gaussian pdf.
    sigma : scalar
        standard deviation of the gaussian pdf.
    validate_pdf : bool, optional
        whether to check that the underlying function is a pdf. The default is False.

    Returns
    -------
    p_gauss : (N,1) array-like
        probability values for x.

    """
    p_gauss = norm.pdf(x=x, loc=mu, scale=sigma)
    if validate_pdf == True:
        min_x = -np.inf
        max_x = np.inf
        integral, _ = integrate.quad(
            lambda x: norm.pdf(x=x, loc=mu, scale=sigma), min_x, max_x
        )
        print("validation that gauss func used is pdf:")
        print(
            f" => integral over the interval [{min_x},{max_x}] using mu={mu} and sigma={sigma} was: {integral}"
        )
    return p_gauss


def calc_von_mises_probability_density(theta, mu, kappa, validate_pdf=False):
    """
    Calculate probability densities for given set of angles theta
    from a von-mises probability density function
    with mean mu and concentration kappa.

    (this function is a wrapper for scipy.stats.vonmises.pdf())

    Parameters
    ----------
    theta : (N,1) array-like
        the angles for which to calculate probability densities.
    kappa : scalar
        the concentration parameter (approx 1/std) of the von-mises pdf.
    mu : scalar
        the expected value of the von-mises pdf.
    validate_pdf : bool, optional
        whether to check that the underlying function is a pdf. The default is False.

    Returns
    -------
    p_vonmises : (N,1) array-like
        probability values for theta.

    """
    p_vonmises = vonmises.pdf(x=theta, kappa=kappa, loc=mu)
    if validate_pdf == True:
        min_theta = -np.pi
        max_theta = np.pi
        integral, _ = integrate.quad(
            lambda x: (vonmises.pdf(x=x, kappa=kappa, loc=mu)),
            min_theta,
            max_theta,
        )
        print("validation that von mises func used is pdf:")
        print(
            f" => integral over the interval [{np.rad2deg(min_theta)}°,{np.rad2deg(max_theta)}°] using mu={np.rad2deg(mu)}° and kappa={kappa} was: {integral}"
        )
    return p_vonmises


def calc_gamma_probability_density(x, alpha, beta, validate_pdf=False):
    """
    Calculate probability densities for given set of points x
    from a gamma probability density function
    with shape parameter alpha and scale parameter beta.

    (this function is a wrapper for scipy.stats.gamma.pdf())

    Parameters
    ----------
    x : (N,1) array-like
        the points for which to calculate probability densities.
    alpha : scalar
        the shape paramater of the gamma pdf.
    beta : scalar
        the scale parameter of the gamma pdf.
    validate_pdf : bool, optional
        whether to check that the underlying function is a pdf. The default is False.

    Returns
    -------
    p_gamma : (N,1) array-like
        probability values for x.

    """

    if np.min(x) < 0:
        print(
            "Warning: you have provided x values < 0, but the used gamma distribution is bounded at 0!"
        )
    p_gamma = gamma.pdf(x=x, a=alpha, scale=1 / beta)
    if validate_pdf == True:
        min_x = 0
        max_x = np.inf
        integral, _ = integrate.quad(
            lambda x: (gamma.pdf(x=x, a=alpha, scale=1 / beta)), min_x, max_x
        )
        print("validation that gamma func used is pdf:")
        print(
            f" => integral over the interval [{min_x},{max_x}] using alpha={alpha} and beta={beta} was: {integral}"
        )
    return p_gamma


def _calc_gamma_von_mises_combined(
    x, alpha, beta, theta, mu, kappa, validate_pdf=False
):
    p_vonmises = calc_von_mises_probability_density(theta, mu, kappa)
    p_gamma = calc_gamma_probability_density(x, alpha, beta)

    if validate_pdf == True:
        min_theta = -np.pi
        max_theta = np.pi
        min_x = 0
        max_x = np.inf
        # format func for integration
        func = lambda theta, x: calc_von_mises_probability_density(
            theta=theta, mu=mu, kappa=kappa
        ) * calc_gamma_probability_density(x=x, alpha=alpha, beta=beta)
        # get double integral
        integral, _ = integrate.dblquad(
            func=func,  # note: dblquad takes func(y,x)!
            a=min_x,  # lim for y
            b=max_x,
            gfun=lambda x: min_theta,  # lim for x
            hfun=lambda x: max_theta,
        )

        print("validation that combined von mises and gamma is pdf:")
        print(
            f" => integral over the interval x: [{min_x},{max_x}]"
            + f" and theta: [{np.rad2deg(min_theta):.4f}°,{np.rad2deg(max_theta):.4f}°]"
            + f" using alpha={alpha:.4f}, beta={beta:.4f}"
            + f" and mu={np.rad2deg(mu):.4f}°, kappa={kappa:.4f} was: {integral:.4f}"
        )
    return p_vonmises * p_gamma


def _calc_gamma_gamma_combined(
    x1, alpha1, beta1, x2, alpha2, beta2, validate_pdf=False
):
    p_gamma1 = calc_gamma_probability_density(x1, alpha1, beta1)
    p_gamma2 = calc_gamma_probability_density(x2, alpha2, beta2)

    if validate_pdf == True:
        min_x = 0
        max_x = np.inf
        # format func for integration
        func = lambda x1, x2: calc_gamma_probability_density(
            x=x1, alpha=alpha1, beta=beta1
        ) * calc_gamma_probability_density(x=x2, alpha=alpha2, beta=beta2)
        # get double integral
        integral, _ = integrate.dblquad(
            func=func,  # note: dblquad takes func(y,x)!
            a=min_x,  # lim for y
            b=max_x,
            gfun=lambda x: min_x,  # lim for x
            hfun=lambda x: max_x,
        )

        print("validation that combined gamma and gamma is pdf:")
        print(
            f" => integral over the interval x: [{min_x},{max_x}]"
            + f" using alpha1={alpha1:.4f}, beta1={beta1:.4f}"
            + f" using alpha2={alpha2:.4f}, beta2={beta2:.4f} was: {integral:.4f}"
        )
    return p_gamma1 * p_gamma2


def von_mises_flat():
    kappa_flat = np.finfo(np.float32).eps  # smallest nonzero value
    mu_0 = 0  # for both gaussian and von mises
    return mu_0, kappa_flat


def gauss_flat():
    mu_0 = 0  # for both gaussian and von mises
    sigma_flat = np.finfo(np.float32).max  # largest representable value
    return mu_0, sigma_flat


def gamma_flat():
    alpha_flat = 1 / 3  # after Kerman 2011 (fig 4)
    beta_flat = np.finfo(
        np.float32
    ).eps  # should be 0 after Kerman 2011 (fig 4)
    return alpha_flat, beta_flat
