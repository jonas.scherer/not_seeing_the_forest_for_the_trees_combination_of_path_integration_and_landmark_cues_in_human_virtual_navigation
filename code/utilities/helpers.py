"""
:AUTHOR: Jonas Scherer
:ORGANIZATION: Department of Neurobiology and Department of Cognitive Neuroscience, Bielefeld University, Germany
:CONTACT: jonas.scherer@uni-bielefeld.de
:SINCE: Mon Dec 12 11:08:00 2022
:VERSION: 0.1

This module contains helper functions.
"""

import numpy as np
import pandas as pd
import os
import json
import glob
import re
from matplotlib import pyplot as plt
from utilities import error_measures

def load_csv_as_dfmi(filename, idx_col=None, n_skiprows=0):
    """
    Load the contents of a .csv file as a (multi-indexed) dataframe.

    Parameters
    ----------
    filename : string
        the name fo the .csv file to be loaded.
    idx_col : list of strings
        which columns to use as index columns for the output dataframe.
    n_skiprows : int, optional
        how many rows to skip at the top of the file. The default is 0.

    Returns
    -------
    df : pandas dataframe
        the dataframe created from the .csv file.

    """
    df = pd.read_csv(filename, index_col=idx_col, skiprows=n_skiprows)
    return df

def load_data_from_json(filename):
    """
    Load data from a .json file and import it as a python object.

    Parameters
    ----------
    filename : string
        the name of the .json file to be loaded.

    Returns
    -------
    loaded_data : python object
        the loaded data.
    """
    file = open(f"{filename}.json", "r")
    json_str = file.read()
    file.close()
    loaded_data = json.loads(json_str)
    return loaded_data

def create_trial_settings_dict_from_session_json(filename):
    """
    function creates dict that includes session information from .json settings file for all
    trials and conditions

    Parameters
    ----------
    filename : str
        filename with path of the .json settings file (without .json)

    Returns
    -------
    out : dict
        dict that contains settings and object parameters of all trials and conditions

    """
    # cut ".json" if it was provided
    if ".json" in filename[-5:]:
        filename = filename[0:-5]
    # read json string
    sess = load_data_from_json(filename)
    # navigate to trialsettings list
    trialsettings_list = sess["Blocks"][0]["TrialSettings"]
    # loop through list and append data to out dict if condition is not present yet
    out = {}
    for setting in trialsettings_list:
        key = setting["Name"]
        # if key in out:
        #     print(
        #         f'Found duplicate trial name "{key}"'
        #         + " in list of session settings.\n"
        #         + "Assuming data is identical"
        #         + " and overwriting previous entry..."
        #     )
        out[key] = setting
    return out

def get_framerate(timeseries, measure="median"):
    """
    Function to get median or mean framerate from pandas dataframe or array-like object.

    Parameters
    ----------
    timeseries : array-like or pd.DataFrame 
        data with Timestamp Index or column named "timestamps"
    measure : string ("median", or "mean")
        measure for central tendency to apply (default="median")
    

    Returns
    -------
    framerate : numeric
        median or mean framerate of the input array-like
    """
    # get time series from dataframe
    if type(timeseries) == pd.core.frame.DataFrame:
        if (
            type(list(timeseries.index)[0])
            == pd._libs.tslibs.timestamps.Timestamp
        ):
            timeseries = list(timeseries.index)
        elif any(timeseries.columns == "timestamps"):
            timeseries = list(timeseries.timestamps)
        elif any(timeseries.columns == "Timestamps"):
            timeseries = list(timeseries.Timestamps)
        else:
            msg = "Can't find timestamps in input DataFrame. Please make it a pandas DatetimeIndex or a <timestamp> column in the dataframe."
            raise ValueError(msg)
    # make time series a Timestamp array with certain format, see below
    if not isinstance(timeseries[0], pd._libs.tslibs.timestamps.Timestamp):
        timeseries = list(
            pd.to_datetime(timeseries, format="%d/%m/%Y %H:%M:%S.%f")
        )
    diffs = np.diff(timeseries)
    diffs_s = np.array(
        [diffs[x] / np.timedelta64(1, "s") for x in range(len(diffs))]
    )
    # remove instances where diffs_s is zero
    diffs_s = diffs_s[[d > 0.001 for d in diffs_s]]
    framerate = 1 / diffs_s
    if measure == "median":
        framerate = np.median(framerate)
    elif measure == "mean":
        framerate = np.mean(framerate)
    else:
        msg = "Unknown input for measure"
        raise KeyError(msg)
    return framerate

def get_object_positions_for_condition(
    settingsdict, condition, object_type="LandmarkPositions"
):
    """
    function extracts object positions of either Landmarks or Waypoints in a certain experimental condition
    and returns them as a pandas DataFrame.

    Parameters
    ----------
    settingsdict : dict
        dict containing information about experimental settings as returned by function "create_trial_settings_dict_from_session_json"
    condition : str
        specific experimental condition that is represented in settingsdict
    object_type : str
        "LandmarkPositions" or "WaypointPositions"

    Returns
    -------
    df : pandas DataFrame
        contains positions of landmarks or waypoints

    """
    poslist = settingsdict[condition][object_type]
    index = np.arange(0, len(poslist))
    df = pd.DataFrame(index=index, columns=["X", "Y"])
    for n in index:
        df.at[n, "X"] = poslist[n]["X"]
        df.at[n, "Y"] = poslist[n]["Z"]
    return df

def get_performance_dict(datapath, colormap="viridis", colormap_part=[0,1], measure="median"):
    """
    sorts participants by their performance (position error) in 99 object condition, creates a colormap,
    and assigning each participant to a color in the colormap according to their performance, returning sequence as a dictionary

    Parameters
    ----------
    datapath : str
        full path to th experimental results DataFrame
    colormap : str, optional
        string defining colormap from matplotlib.pyplot.cm, defaults to "viridis"
    colormap_part : list with length 2
        defines what portion of the colormap to use
    measure : "median" or "var"
        defines whether performance for median position error or variance of position error is used as performance score

    Returns
    -------
    color_dict : dictionary
        dictionary with keys; assignment of participant IDs and color
    colormap : numpy array
        raw colormap chosen with the length equal to number of participants
    performance_dict : dictionary
        holding for each participant ID the ranking integer
    """
    # read in experiment result dataframe
    experiment_results = pd.read_csv(datapath, index_col=0)  
    # group by condition ("num_objects") and participant ("ppid")
    grouped = experiment_results.groupby(["num_objects","ppid"])
    # get performance score to sort by later
    if measure == "median":
        # get median position error to sort by later
        grouped_errors = grouped["err_pos_walk"].median()
    elif measure == "var":
        # get variance position error to sort by later
        grouped_errors = grouped["err_pos_walk"].std()
    # remove MultiIndex for easy handling
    grouped_errors = grouped_errors.reset_index()
    # select participants' position errors from forest condition (99 objects)
    forest_condition = grouped_errors.loc[grouped_errors.num_objects==99]
    # sort participant IDs by performance in forest condition
    ppid_forest_performance = forest_condition.sort_values("err_pos_walk").ppid.values
    # get colormap that is as long as number of participants
    n_ppids = len(np.unique(experiment_results.ppid))
    colormap = plt.cm.get_cmap(colormap, np.ceil(n_ppids*(2-colormap_part[1]+colormap_part[0])))(np.arange(0,n_ppids)+ int(np.round(colormap_part[0]*n_ppids)))
    # make dictionary that assigns certain color to each participant according to performance in forest condition
    color_dict = dict(zip(ppid_forest_performance, colormap))
    # additionally make a dictionary that holds each participant's ranking
    performance_dict = dict(zip(ppid_forest_performance, np.arange(1, n_ppids+1)))
    
    return {"color_dict": color_dict, "colormap": colormap, "performance_dict": performance_dict}


def calc_error_measures_for_LM(
    df, obj_pos, name_suffix, convertToDegrees=True
):
    """
    Calculate distance, angle and position error relative to a given location
    in the environment (usually a landmark object)

    Parameters
    ----------
    df : pandas dataframe
        the dataset from a single participant contained in a pandas dataframe.
    obj_pos : (1,2) array-like
        the position on which to centre the error measures.
    name_suffix : string
        the suffix used for output column names
        (pattern is: "measure_name_" + name_string).
    convertToDegrees : boolean, optional
        a flag to determine whether angle error should be in degrees or radian.
        The default is True.

    Returns
    -------
    df : pandas dataframe
        the input dataframe with added columns for the calculated measures

    """
    goal = pd.concat([df["goalTrueX"], df["goalTrueZ"]], axis=1)
    start = pd.DataFrame(data=np.tile(obj_pos, (len(goal), 1)), dtype="float")
    walked = pd.concat([df["goalEstimateX"], df["goalEstimateZ"]], axis=1)

    observedVect, targetVect = error_measures.calculate_vectors(goal, start, walked)
    dist_err_LM = error_measures.calc_distance_error(
        np.array(observedVect), np.array(targetVect)
    )
    dir_err_LM = error_measures.calc_directional_error(
        np.array(observedVect), np.array(targetVect)
    )
    pos_err_LM, _ = error_measures.calc_position_error(
        np.array(observedVect), np.array(targetVect)
    )

    if convertToDegrees:
        dir_err_LM = np.degrees(dir_err_LM)
    df.loc[:, "err_dist_" + name_suffix] = dist_err_LM
    df.loc[:, "err_dir_" + name_suffix] = dir_err_LM
    df.loc[:, "err_pos_" + name_suffix] = pos_err_LM

    return df

def import_dataset(ID, rootpath):
    """
    function imports datasets included in "session_results_*.csv" and "traj_player_*.csv" files with the participant's ID
    and returns the content as dataframes.

    Parameters
    ----------
    ID : str
        participant's ID in our standard format "XX##_YY##'
    rootpath : str
        path that includes "session_results_*.csv" and "traj_player_*.csv" files

    Returns
    -------
    df_sess : pandas DataFrame
        content of the "session_results_*.csv" file
    df_player : pandas DataFrame
        content of the "traj_player_*.csv" file
    """
    # generate filenames
    name_sess = os.path.join(rootpath, f"processed_data.csv")
    name_traj_player = os.path.join(rootpath, "trajectories", "processed", f"traj_player_{ID}.csv")
    # define csv format
    idx_columns = [0, 1, 2, 3]
    n_rows_to_skip = 1
    # load data as multi-index df
    df_sess = pd.read_csv(name_sess, index_col=idx_columns)
    df_sess = df_sess.loc[df_sess.ppid==ID]
    df_player = pd.read_csv(name_traj_player, index_col=idx_columns)
    
    return df_sess, df_player