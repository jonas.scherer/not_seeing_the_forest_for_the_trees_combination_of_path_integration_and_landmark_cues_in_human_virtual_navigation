"""
:AUTHORS: Martin M. Müller, Olivier J. N. Bertrand
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: martin.mueller@uni-bielefeld.de
:SINCE: Wed Nov 24 16:13:28 2021
"""
import numpy as np
from scipy.optimize import minimize_scalar
from scipy.special import i0
from scipy.special import i1
from scipy.stats import circmean, circstd


def findkappa(data, verbose=False):
    cstd = circstd(data)
    kappa = (1 / cstd) ** 2
    if kappa >= 100:
        if verbose:
            print(
                f"approximated kappa was {kappa:0.3f}, skipping minimisation!"
            )
    if kappa < 100:
        # We need to refine
        cstd = circstd(data)
        cs = 1 - (cstd**2) / 2

        def f(x):
            return ((i1(x) / i0(x)) - cs) ** 2

        result = minimize_scalar(f, options={"maxiter": 100})
        if result.success == False:
            if verbose:
                print(
                    "minimisation of kappa was not successful, using transformed circstd instead!"
                )
        if result.success == True:
            if verbose:
                print(
                    f"minimisation of kappa was successful, using minimised value ({result.x:0.3f}) instead of transformed circstd ({kappa:0.3f})!"
                )
            kappa = result.x
    return kappa


# pdf below has analytical solution
# https://www.stat.sfu.ca/content/dam/sfu/stat/alumnitheses/MiscellaniousTheses/Bentley-2006.pdf
def find_mle_von_mises(data):
    """
    Find the maximum likelihood estimates (MLE) of paramaters mu and kappa
    to fit a von mises distribution on the given data.

    Parameters
    ----------
    data : (N,1) array-like
        the data on which to fit the von mises distribution.

    Returns
    -------
    mu : scalar
        mle of the expected value of the von mises distribution.
    kappa : scalar
        mle of the concentration parameter of the von mises distribution.
        (kappa is approx 1/circular std)

    """
    # The two bessel function required for the ratio

    mu = circmean(data, high=np.pi, low=-np.pi)
    kappa = findkappa(data)
    return mu, kappa


def find_mle_gaussian(data):
    """
    Find the maximum likelihood estimates (MLE) of paramaters mu and sigma
    to fit a gaussian on the given data.

    Parameters
    ----------
    data : (N,1) array-like
        the data on which to fit the gaussian.

    Returns
    -------
    mu : scalar
        mle of the expected value of the gaussian.
    sigma : scalar
        mle of the standard deviation of the gaussian.

    """
    mu = np.mean(data)
    sigma = np.std(data)
    return mu, sigma


def find_mle_gamma(data):
    """
    Find the maximum likelihood estimates (MLE) of paramaters alpha and beta
    to fit a gamma distribution on the given data.

    Parameters
    ----------
    data : (N,1) array-like
        the data on which to fit the gamma distribution.

    Parameters
    ----------
    data : TYPE
        DESCRIPTION.

    Returns
    -------
    alpha : scalar
        mle of shape parameter alpha.
    beta : scalar
        mle of inverse scale parameter beta (1/theta).

    """

    n = data.shape[0]
    log_data = np.log(data)

    "According to Ye 2017"
    alpha = n * (np.sum(data))
    alpha /= n * np.sum(data * log_data) - np.sum(log_data) * np.sum(data)

    theta = n * np.sum(data * log_data) - np.sum(log_data) * np.sum(data)
    theta /= n**2

    "Correct according to Louzada 2019"
    theta = n * theta / (n - 1)
    alpha -= (
        3 * alpha
        - (2 / 3) * (alpha / (alpha + 1))
        - (4 / 5) * (alpha / ((1 + alpha) ** 2))
    ) / n
    beta = 1 / theta

    return alpha, beta
