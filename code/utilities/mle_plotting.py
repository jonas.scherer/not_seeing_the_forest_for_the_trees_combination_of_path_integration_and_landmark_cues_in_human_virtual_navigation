"""
:AUTHORS: Martin M. Müller
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: martin.mueller@uni-bielefeld.de
:SINCE: Wed Nov 17 11:35:01 2021
"""
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import ticker
import matplotlib.colors as colors

from utilities import mle_visualise as mlvis


def plot_prediction(mx, my, ampl, ax, cmap="viridis", rasterise=True):
    """
    Create a prediction heatmap on given axis object.

    Parameters
    ----------
    mx : (M,N) array-like
        all x coordinates for which to compute an amplitude.
    my : (M,N) array-like
        all y coordinates for which to compute an amplitude.
    ampl : (M,N) array-like
        the amplitude map to be plotted.
    ax : axis object
        the axis on which to plot.
    cmap : string, optional
        colormap string. The default is "viridis".
    rasterise : bool, optional
        whether to optimise for export as vector graphic. The default is True.
    Returns
    -------
    mesh : pcolormesh
        the pcolormesh object.

    """
    mesh = ax.pcolormesh(
        mx,
        my,
        ampl,
        shading="nearest",
        cmap=cmap,
        rasterized=rasterise,
        zorder=0,
    )
    ax.set_aspect("equal")
    ax.grid(alpha=0.2)
    return mesh


def plot_setup(goal_pos, home_start_pos, obj_pos, ax, obj_color="g"):
    """
    Plot the relevant points for the condition to given axis.

    Parameters
    ----------
    goal_pos : (1, 2) array-like
        the position of the goal.
    home_start_pos : (1, 2) array-like
        the position of the start of the return leg.
    obj_pos : (N, 2) pandas datafrane with columns 'x' and 'y'
        the positions of the objects in the scene.
    ax : axis object
        the axis on which to plot.

    Returns
    -------
    None.

    """
    ax.scatter(
        goal_pos[0], goal_pos[1], marker="x", color="#b03010", s=80, zorder=50
    )
    ax.scatter(
        home_start_pos[0],
        home_start_pos[1],
        marker="D",
        color="#555555",
        facecolor="w",
        zorder=50,
        s=80,
    )
    ax.scatter(
        -25, 25, marker="D", color="#555555", facecolor="w", zorder=50, s=80
    )
    ax.scatter(
        obj_pos["x"], obj_pos["y"], c=obj_color, marker="^", zorder=5, s=180
    )


def set_axes(
    ax, xmin, xmax, ymin, ymax, tickstep=20, hide_x=True, hide_y=True
):
    """
    Format subplot axes.

    Parameters
    ----------
    ax : axis object
        the axis on whicht o plot.
    xmin : scalar
        lower bound of x-axis.
    xmax : scalar
        uperr bound of x-axis.
    ymin : scalar
        lower bound of y-axis.
    ymax : scalar
        upper bound of y-axis.
    tickstep : scalar, optional
        the axis tick interval. The default is 20.
    hide_x : bool, optional
        whether to hide tick labels on x-axis. The default is True.
    hide_y : bool, optional
        whether to hide tick labels on y-axis. The default is True.

    Returns
    -------
    None.

    """
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.xaxis.set_ticks(np.arange(xmin + tickstep, xmax, tickstep))
    ax.yaxis.set_ticks(np.arange(ymin + tickstep, ymax, tickstep))
    if hide_x:
        ax.set_xticklabels([])
    if hide_y:
        ax.set_yticklabels([])


def create_textbox(ax, text):
    """
    Create a info textbox in the top left corner of the subplot.

    Parameters
    ----------
    ax : axis object
        the axis on which to plot.
    text : string
        the text displayed in the infobox.

    Returns
    -------
    None.

    """
    props = dict(boxstyle="round", facecolor="white", alpha=0.5)

    ax.text(
        0.05,
        0.95,
        text,
        transform=ax.transAxes,
        fontsize=8,
        verticalalignment="top",
        bbox=props,
    )


def create_validation_plot(data, func, param1, param2, ax):
    # define bounds
    (lb, ub) = _derive_bounds(data)
    # create pdf
    x = np.linspace(lb, ub, 100)
    y = func(x, param1, param2)
    # plot pdf and data
    ax2 = ax.twinx()
    ax2.plot(x, y, alpha=0.5, color="red")
    ax.hist(data, bins=10, range=(lb, ub))


def _derive_bounds(data, padding=0.1):
    lb = np.min(data) - (np.mean(data) * padding)
    ub = np.max(data) + (np.mean(data) * padding)
    return (lb, ub)


def plot_endpoints(data, ax):
    endpoints_x = data.goalEstimateX
    endpoints_y = data.goalEstimateZ
    ax.scatter(
        x=endpoints_x,
        y=endpoints_y,
        s=15,
        color="b",
        marker=".",
        alpha=0.7,
    )


def create_amplitude_map(
    x,
    y,
    goal_vect,
    goal_ang,
    model_df,
    model_name_dist,
    model_name_ang,
    mode="gamma",
):
    [mx, my] = np.meshgrid(x, y)
    # shift origin to home start for amplitude map calc
    x_plot = x + goal_vect[0]
    y_plot = y + goal_vect[1]

    # create meshgrid for new coords
    [mx_plot, my_plot] = np.meshgrid(x_plot, y_plot)

    "extract params for plot"
    (mu_vm, kappa_vm) = model_df[
        model_df["model"] == model_name_ang
    ].parameters.values[0]

    """flip angle, since for us left was negative, 
    but for the von mises function negative mu is cw rot from pos x axis,
    and positive mu is ccw rot from pos x-axis.
    """
    mu_vm_shifted = mu_vm * -1
    """finally, set the goal axis as the centre for our vm 
    and then add the fitted angle relative to that"""
    mu_plot = goal_ang + mu_vm_shifted

    if mode == "gaussian":
        (mu, sigma) = model_df[
            model_df["model"] == model_name_dist
        ].parameters.values[0]
        ampl = mlvis.create_probability_map_gauss_banana(
            mx_plot, my_plot, mu_plot, kappa_vm, mu, sigma
        )
    elif mode == "gamma":
        (alpha, beta) = model_df[
            model_df["model"] == model_name_dist
        ].parameters.values[0]
        ampl = mlvis.create_probability_map_gamma_banana(
            mx_plot, my_plot, mu_plot, kappa_vm, alpha, beta
        )

    return ampl


def create_amplitude_map_flat(x, y, goal_vect):
    [mx, my] = np.meshgrid(x, y)
    # shift origin to home start for amplitude map calc
    x_plot = x + goal_vect[0]
    y_plot = y + goal_vect[1]

    # create meshgrid for new coords
    [mx_plot, my_plot] = np.meshgrid(x_plot, y_plot)
    ampl = mlvis.create_probability_map_uniform(mx_plot, my_plot)
    return ampl
