"""
:AUTHORS: Martin M. Müller
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: martin.mueller@uni-bielefeld.de
:SINCE: Mon Nov 29 10:55:11 2021
"""

import numpy as np

def cart2pol(x, y):
    """
    Convert cartesian coordinates to polar.
    The returned angle phi is in the interval [-pi, pi]

    Parameters
    ----------
    x : scalar
        the x coordinate.
    y : scalar
        the y coordinate.

    Returns
    -------
    r : scalar
        the radial distance.
    phi : scalar
        the polar angle.
        the.

    """
    r = np.sqrt(x ** 2 + y ** 2)
    phi = np.arctan2(y, x)
    return r, phi


def pol2cart(r, phi):
    """
    Convert polar coordinates to cartesian.

    Parameters
    ----------
    r : scalar
        the radial distance.
    phi : scalar
        the polar angle in radian.

    Returns
    -------
    x : scalar
        the x coordinate.
    y : scalar
        the y coordinate.

    """
    x = r * np.cos(phi)
    y = r * np.sin(phi)
    return x, y


def rotation_matrix_2d(theta):
    """
    Create a 2d rotation matrix for the desired counter-clockwise rotation angle.

    Parameters
    ----------
    theta : scalar
        the desired counter-clockwise rotation angle in radian.

    Returns
    -------
    r : (2,2) numpy array
        the rotation matrix.

    """
    r = np.array(
        ((np.cos(theta), -np.sin(theta)), (np.sin(theta), np.cos(theta)))
    ).squeeze()
    return r
