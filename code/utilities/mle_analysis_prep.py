"""
:AUTHORS: Martin M. Müller
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: martin.mueller@uni-bielefeld.de
:SINCE: Thu Jan  6 11:04:49 2022
"""
import re
import pandas as pd
import os
import numpy as np
from IPython import get_ipython

from utilities import mle_utils as mlutil
from utilities import helpers
from utilities import mle_player_performance_analysis as analyse


def import_and_analyse(ID, root):
    df_sess, df_traj = helpers.import_dataset(ID=ID, rootpath=root)
    df_sess = analyse.analyse_dataset(df_sess)
    df_sess = df_sess.reset_index(level="trial_name")

    return df_sess, df_traj


def prep_vars(root=""):
    experiment = "not_see_the_forest_for_the_trees"
    conditions = [
        "0_obj_right",
        "1_obj_right",
        "2_obj_right",
        "3_obj_right",
        "10_obj_right",
        "99_obj_right",
    ]
    "define participants to consider"
    results = pd.read_csv(os.path.join(root, "processed_data.csv"))
    IDs = np.unique(results.ppid)
    # define relevant points
    goal_pos = [0, 0]
    home_start_pos = [0, 25]
    waypoint_pos = [-25, 25]
    # derive values
    goal_vect = np.subtract(goal_pos, home_start_pos)
    goal_dist, goal_ang = mlutil.cart2pol(goal_vect[0], goal_vect[1])

    sessiondict = helpers.create_trial_settings_dict_from_session_json(
        os.path.join(root, "experiment_settingsfile")
    )
    obj_pos = get_object_positions_for_condition(sessiondict, "99_obj_right")
    return (
        experiment,
        conditions,
        root,
        IDs,
        goal_pos,
        home_start_pos,
        waypoint_pos,
        goal_vect,
        goal_dist,
        goal_ang,
        obj_pos,
    )


def load_datasets_to_dict(IDs, datapath, ID_prefix=""):
    """
    Load participant datasets and collect them in a dictionary.

    Parameters
    ----------
    IDs : list of strings
        The participant IDs (without prefix).
    datapath : string
        the path to of the source directory from which to load the data.
    ID_prefix : string, optional
        a prefix for the ID strings (like 'MM04_'). The default is "".

    Returns
    -------
    alldata : dictionary
        a dictionary containing each individual dataset as a dfmi.

    """
    alldata = {}
    for ID in IDs:
        "clean up workspace"
        get_ipython().magic("reset -sf")

        "load data"
        ID = ID_prefix + ID
        fname = os.path.join(datapath, f"processed_data.csv")
        idx_columns = [0, 1, 2, 3]
        n_rows_to_skip = 1
        data = pd.read_csv(fname, index_col=idx_columns)
        data = data.loc[data.ppid==ID]

        alldata[ID] = data
    return alldata


def get_object_positions_for_condition(
    settingsdict, condition, object_type="LandmarkPositions"
):
    """
    function extracts object positions of either Landmarks or Waypoints
    in a certain experimental condition
    and returns them as a pandas DataFrame.

    Parameters
    ----------
    settingsdict : dict
        dict containing information about experimental settings
        as returned by function "create_trial_settings_dict_from_session_json"
    condition : str
        specific experimental condition that is represented in settingsdict
    object_type : str
        "LandmarkPositions" or "WaypointPositions"

    Returns
    -------
    df : pandas DataFrame
        contains positions of landmarks or waypoints

    """
    poslist = settingsdict[condition][object_type]
    index = np.arange(0, len(poslist))
    df = pd.DataFrame(index=index, columns=["x", "y"], dtype="float")
    for n in index:
        df.at[n, "x"] = poslist[n]["X"]
        df.at[n, "y"] = poslist[n]["Z"]
    return df