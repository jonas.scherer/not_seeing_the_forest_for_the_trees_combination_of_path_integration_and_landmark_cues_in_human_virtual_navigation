"""
:AUTHOR: Jonas Scherer
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: jonas.scherer@uni-bielefeld.de
:SINCE: Tue Dec 13 17:57:00 2022
:VERSION: 0.1

This module contains helper functions for plotting figures used in the virtual forest publication.
"""
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

from scipy.stats import circmean

from utilities import helpers
from utilities import error_measures

def prep_condition_figs(
    settingsdict, conditions, title_add="",
):
    """
    function prepares an empty figure for each experimental condition defined by conditions.
    It scatters object positions and waypoint positions and creates an xy-coordinate system
    with appropriate labels.

    Parameters
    ----------
    conditions : list of str
        list of strings containing the experimental conditions to create figures for. conditions need to be
        represented in settingsdict.

    Returns
    -------
    figs : list of matplotlib figs
         figs contains one fig for each experimental condition figure
    axs : list of matplotlib axes
        axs contains one axis for each experimental condition figure

    """
    print("...preparing figure panels for different conditions...")
    if (type(title_add) == list) | (type(title_add) == np.ndarray):
        if len(title_add) != len(conditions):
            msg = f"conditions {len(conditions)} and title_add {len(title_add)} must have the same length"
            raise KeyError(msg)

    fig, axs = plt.subplots(1, len(conditions))        
    for cond in range(len(conditions)):
        if type(title_add) == list:
            Title_add = title_add[cond]
        else:
            Title_add = title_add
        if type(axs) == type(None):
            _, ax = plt.subplots()
        else:
            ax = axs[cond]
        # scatter objects, waypoints and true target
        objpos = helpers.get_object_positions_for_condition(
            settingsdict, conditions[cond], object_type="LandmarkPositions"
        )
        waypos = helpers.get_object_positions_for_condition(
            settingsdict, conditions[cond], object_type="WaypointPositions"
        )
        objpos.plot.scatter(
            x="X",
            y="Y",
            ax=ax,
            s=150,
            color="#006e00",
            marker="^",
            zorder=50,
        )
        waypos.plot.scatter(
            x="X",
            y="Y",
            ax=ax,
            s=40,
            marker="o",
            color="#858585",
            zorder=30,  #
        )
        axs[cond].scatter(
            x=0, y=0, s=50, marker="x", color="#b03010", zorder=90
        )
        
    return fig, axs 

def get_traj_plotting_df(df):
    """
    function prepares multiindex of dataframe for plotting and reduces it to the necessary
    x,y and trial_state columns only.

    Parameters
    ----------
    df : pandas DataFrame
        dataframe contains trajectory information from 'traj_player_*.csv' file
        in particular

    Returns
    -------
    df_traj : pandas DataFrame
        reduced dataframe with prepared multiindex
    """
    df = df.set_index(["session_num", "trial_name", "trial_num"])  # put trial_number in multiindex
    # make new reduced dataframe
    df_traj = pd.DataFrame(columns=["X", "Y", "trial_state", "flagged"])
    df_traj["X"] = df.pos_x
    df_traj["Y"] = df.pos_z
    df_traj["trial_state"] = df.trial_state
    df_traj["flagged"] = df.flagged
    df_traj.set_index = df.index

    return df_traj

def plot_trial_trajs(
    df_traj,
    fig,
    axs,
    conditions,
    scatter_color="black",
    ):
    """
    function plots trajectories of each trial by filling figures prepared by function 'prep_condition_figs'
    and separating experimental conditions.
    Additionally trajectories can be annotated with their respective trial number and
    the central tendency of trajectory endpoints can be plotted as median or circular mean.

    Parameters
    ----------
    df : pandas DataFrame
        dataframe that contains a MultiIndex in format ['session_num','block_num','trial_name','trial_num']
        and columns "trial_state", "X" and "Y" representing coordinates of trajectories
    axs : list of matplotlib axes
        len(axs) needs to be len(conditions)
    conditions : list of str
        list of strings containing the experimental conditions to create figures for. conditions need to be
        represented in settingsdict.
    annotate_trial_no : boolean
        True - total trial numbers are annotated right besides the trajectory endpoints /
        False - no annotation
    plot_central_direction : boolean
        False - no central vector is plotted / "median" or "mean" plots vector representing central tendency direction and distance / True - invalid / only mean is circular statistics, median is not
    radians : boolean
        True - angles in the provided dataframes are in radians
        False - angles are in degrees
    """
    print("...plotting all trajectories and endpoints...")
    # find unique indices that separate conditions and trials from each other
    trial_ind = df_traj.index.unique()
    # define colormap
    cond_idx = list(trial_ind.names).index("trial_name") # position in index where condition is indicated
    for running_no, t in enumerate(trial_ind):
        #print(f"plotting trial {running_no + 1} of {len(trial_ind)}")
        cond = t[cond_idx]  # only plot conditions that are listed in script
        if cond not in conditions:
            continue
        trial = df_traj[df_traj.index == t]
        displacement = trial[
            (trial.trial_state == "Displacement") & (trial.index == t)
        ]
        homing = trial[
            (trial.trial_state == "Homing") & (trial.index == t)
        ]
        axs_ref = conditions.index(cond)
        displacement.plot(
            x="X",
            y="Y",
            ax=axs[axs_ref],
            color="#505050",
            linewidth=0.5,
            linestyle="dotted",
            alpha=0.1,
            legend=False,
            zorder=10,
        )
        homing.plot(
            x="X",
            y="Y",
            ax=axs[axs_ref],
            color="#505050",
            linewidth=1,
            linestyle="dotted",
            alpha=0.1,
            legend=False,
            zorder=70,
        )
        # some trials have only displacement or only homing for a reason
        if len(homing) == 0:
            continue
        axs[axs_ref].scatter(
            x=homing["X"].iloc[-1],
            y=homing["Y"].iloc[-1],
            marker="o",
            s=80, # scatter_size
            facecolor=scatter_color,
            edgecolor=None,
            lw=0,
            alpha=1,
        )
    
    return fig, axs
