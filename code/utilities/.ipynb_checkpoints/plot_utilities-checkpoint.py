"""
:AUTHOR: Jonas Scherer
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: jonas.scherer@uni-bielefeld.de
:SINCE: Tue Dec 13 17:57:00 2022
:VERSION: 0.1

This module contains functions to plot figures used in the virtual forest publication.
"""
import pandas as pd
import numpy as np

from matplotlib import pyplot as plt

from utilities import plot_helpers
from utilities import helpers
from utilities import cleaning_utilities


def create_traj_plots(
    trajectories_path,
    IDs,
    settingsdict,
    pool_IDs=False,
    ID_colors=None,
    annotate_trial_no=False,
    plot_central_tendency=False,
    subplot_titles=None,
    xticks=None,
    xticklabels=None,
    yticks=None,
    yticklabels=None,
):
    """
    function plots trajectories of participants in experiment MM04 separated by their experimental condition.
    Parameter 'merge_IDs' allows to pool all participants' trajectories.
    Parameters
    ----------
    root : str
        root path to where 'session_results_*.csv' files are located
    IDs : list of str
        list of participant IDs taht can be found in root folder
    settingsdict : dict
        dictionary that contains experimental settings for all given conditions. Can be acquired from function 'create_trial_settings_dict_from_session_json'
    savepath : str
        path where created figures are saved
    pool_IDs : boolean
        False - Figures are separated for participants / True - All participants are pooled
    annotate_trial_no : boolean
        False - trial numbers are not put besides endpoints / True - Trial numbers are plotted as text besides trial endpoints
    plot_central_direction : boolean
        False - no central vector is plotted / "mean" plots vector representing central tendency direction and distance (in circular statistic) / "median" plots vector representing central tendency direction and distance (no circular statistic) / True - invalid
    """
    if (
        pool_IDs
    ):  # if all IDs are pooled, then prepare empty figures only once for all IDs
        conditions = list(settingsdict.keys())
        fig, axs = plot_helpers.prep_condition_figs(
            settingsdict=settingsdict,
            conditions=conditions,
            title_add="pooled participants",
        )
    # convert ID to list if string is provided
    if (type(IDs) != list) & (type(IDs) != np.ndarray):
        IDs = [IDs]
    for idx, ID in enumerate(IDs):
        conditions = list(settingsdict.keys())

        df_play = pd.read_csv(trajectories_path.format(ID))

        if (
            not pool_IDs
        ):  # if IDs are not pooled, then prepare empty figures for each ID
            plt.close("all")
            fig, axs = plot_helpers.prep_condition_figs(
                settingsdict=settingsdict, conditions=conditions, title_add=ID
            )
        df_traj = plot_helpers.get_traj_plotting_df(
            df=df_play
        )  # prepare trajectory df for plotting

        ID_color = "black"
        if type(ID_colors) != type(None):
            ID_colors = np.asarray(ID_colors)
            if (not ID_colors.shape == (len(IDs), 4)) & (
                not ID_colors.shape == (len(IDs), 3)
            ):
                msg = "length of ID_colors must correspond to length of IDs. Please provide 3 (rgb) or 4 (rgb + alpha) values for each ID"
                raise ValueError(msg)
            ID_color = ID_colors[idx, :]
        fig, axs = plot_helpers.plot_trial_trajs(
            df_traj=df_traj,
            fig=fig,
            axs=axs,
            conditions=conditions,
            scatter_color=ID_color,
        )  # plot trial trajectories

        # adapt axes properties
        if type(subplot_titles) == type(None):
            subplot_titles = conditions
        
        # set axis ticks and labels if not provided in function call
        if (type(xticks)==None) or (type(xticklabels)==None) or (type(yticks)==None) or (type(yticklabels)==None):
            xticks = np.arange(-50, 51, 10)
            xticklabels = ["-50", "", "", "", "", "0", "", "", "", "", "50"]
            yticks = np.arange(-50, 51, 10)
            yticklabels = ["-50", "", "", "", "", "0", "", "", "", "", "50"]
            
        for axis_idx in range(len(axs)):
            axs[axis_idx].set(
                xlabel="",
                ylabel="",
                xlim=[-30, 20],
                ylim=[-20, 30],
            )
            axs[axis_idx].set_title(subplot_titles[axis_idx], fontsize=15)
            axs[axis_idx].set_xticks(xticks)
            axs[axis_idx].set_xticklabels(xticklabels, fontsize=15)
            axs[axis_idx].set_yticks(yticks)
            axs[axis_idx].set_yticklabels(yticklabels, fontsize=15)
            axs[axis_idx].set_aspect("equal", "box")
            axs[axis_idx].grid(visible=True, axis="both", alpha=0.4)
            if axis_idx == 0:
                axs[axis_idx].set_xlabel("x-coordinate [vm]", fontsize=15)
                axs[axis_idx].set_ylabel("y-coordinate [vm]", fontsize=15)
    return fig, axs
