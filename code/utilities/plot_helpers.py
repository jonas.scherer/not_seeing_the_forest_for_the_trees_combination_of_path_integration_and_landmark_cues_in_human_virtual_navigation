"""
:AUTHOR: Jonas Scherer
:ORGANIZATION: Department of Neurobiology and Department of Cognitive Neuroscience, Bielefeld University, Germany
:CONTACT: jonas.scherer@uni-bielefeld.de
:SINCE: Tue Dec 13 17:57:00 2022
:VERSION: 0.1

This module contains helper functions for plotting figures.
"""
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

from scipy.stats import circmean

from utilities import helpers
from utilities import error_measures

def prep_condition_figs(
    settingsdict, conditions,
):
    """
    function prepares an empty figure for each experimental condition defined by conditions.
    It scatters object positions and waypoint positions and creates an xy-coordinate system
    with appropriate labels.

    Parameters
    ----------
    settingsdict: dict
        dictionary holding settings information as output by function helpers.create_trial_settings_dict_from_session_json
    conditions : list of str
        list of strings containing the experimental conditions to create figures for. conditions need to be
        represented in settingsdict.

    Returns
    -------
    figs : list of matplotlib figs
         figs contains one fig for each experimental condition figure
    axs : list of matplotlib axes
        axs contains one axis for each experimental condition figure

    """
    print("...preparing figure panels for different conditions...")

    fig, axs = plt.subplots(2, int(np.ceil(len(conditions) / 2)))
    for cond in range(len(conditions)):
        if type(axs) == type(None):
            _, ax = plt.subplots()
        else:
            ax = axs[np.unravel_index(cond, (axs.shape))]
        # scatter objects, waypoints and true target
        objpos = helpers.get_object_positions_for_condition(
            settingsdict, conditions[cond], object_type="LandmarkPositions"
        )
        waypos = helpers.get_object_positions_for_condition(
            settingsdict, conditions[cond], object_type="WaypointPositions"
        )
        ax.scatter(
            x=objpos.X, 
            y=objpos.Y, 
            marker="^",
            s=75,
            edgecolors="#adc178",
            facecolors="white",
            linewidth=1,
            zorder=30,
        )
        ax.scatter(
            x=waypos.X, 
            y=waypos.Y, 
            marker="D",
            s=30,
            edgecolors="#505050", 
            facecolors="white", 
            linewidth=1,
            zorder=85,
        )
        axs[np.unravel_index(cond, (axs.shape))].scatter(
            x=0, y=0, s=50, marker="x", color="#b03010", zorder=90
        )
        
    return fig, axs 

def get_traj_plotting_df(df):
    """
    function prepares multiindex of dataframe for plotting and reduces it to the necessary
    x,y and trial_state columns only.

    Parameters
    ----------
    df : pandas DataFrame
        dataframe contains trajectory information from 'traj_player_*.csv' file
        in particular

    Returns
    -------
    df_traj : pandas DataFrame
        reduced dataframe with prepared multiindex
    """
    df = df.set_index(["session_num", "trial_name", "trial_num"])  # put trial_number in multiindex
    # make new reduced dataframe
    df_traj = pd.DataFrame(columns=["X", "Y", "trial_state", "flagged"])
    df_traj["X"] = df.pos_x
    df_traj["Y"] = df.pos_z
    df_traj["trial_state"] = df.trial_state
    df_traj["flagged"] = df.flagged
    df_traj.set_index = df.index

    return df_traj

def plot_trial_trajs(
    df_traj,
    fig,
    axs,
    conditions,
    scatter_color="black",
    plot_trajectories=True,
    colored_trajectories=False,
    ):
    """
    function plots trajectories of each trial by filling figures prepared by function 'prep_condition_figs'
    and separating experimental conditions.
    Additionally trajectories can be annotated with their respective trial number and
    the central tendency of trajectory endpoints can be plotted as median or circular mean.

    Parameters
    ----------
    df_traj : pandas DataFrame
        dataframe that contains a MultiIndex in format ['session_num','block_num','trial_name','trial_num']
        and columns "trial_state", "X" and "Y" representing coordinates of trajectories
    fig : matplotlib figure
        if existing, this hands over figure to plot trajectories in
    axs : list of matplotlib axes
        len(axs) needs to be len(conditions)
    conditions : list of str
        list of strings containing the experimental conditions to create figures for. conditions need to be
        represented in settingsdict.
    annotate_trial_no : boolean
        True - total trial numbers are annotated right besides the trajectory endpoints /
        False - no annotation
    plot_central_direction : boolean
        False - no central vector is plotted / "median" or "mean" plots vector representing central tendency direction and distance / True 
        is invalid / only mean is circular statistics, median is not
    radians : boolean
        True - angles in the provided dataframes are in radians
        False - angles are in degrees
    """
    print("...plotting all trajectories and endpoints...")
    # find unique indices that separate conditions and trials from each other
    trial_ind = df_traj.index.unique()
    cond_idx = list(trial_ind.names).index("trial_name") # position in index where condition is indicated
    for running_no, t in enumerate(trial_ind):
        cond = t[cond_idx]  # only plot conditions that are listed in script
        if cond not in conditions:
            continue
        trial = df_traj[df_traj.index == t]
        displacement = trial[
            (trial.trial_state == "Displacement") & (trial.index == t)
        ]
        homing = trial[
            (trial.trial_state == "Homing") & (trial.index == t)
        ]
        axs_ref = conditions.index(cond)
        
        if plot_trajectories:
            if colored_trajectories:
                traj_color = scatter_color
                traj_style = "solid"
                traj_alpha = 1
            else:
                traj_color = "#505050"
                traj_style = "dotted"
                traj_alpha = 0.1
            displacement.plot(
                x="X",
                y="Y",
                ax=axs[np.unravel_index(axs_ref, (axs.shape))],
                color="#505050",
                linewidth=1,
                linestyle="dotted",
                alpha=0.1,
                legend=False,
                zorder=10,
            )
            homing.plot(
                x="X",
                y="Y",
                ax=axs[np.unravel_index(axs_ref, (axs.shape))],
                color=traj_color,
                linewidth=1,
                linestyle=traj_style,
                alpha=traj_alpha,
                legend=False,
                zorder=70,
            )
        # some trials have no homing state, likely caused by too quickly pressing enter
        if len(homing) == 0:
            continue
        axs[np.unravel_index(axs_ref, (axs.shape))].scatter(
            x=homing["X"].iloc[-1],
            y=homing["Y"].iloc[-1],
            marker="o",
            s=60, 
            facecolor=scatter_color,
            edgecolor=None,
            lw=0,
            alpha=1,
            zorder=70
        )
    
    return fig, axs
