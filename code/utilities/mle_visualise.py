"""
:AUTHORS: Martin M. Müller
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: martin.mueller@uni-bielefeld.de
:SINCE: Mon Nov 29 14:15:52 2021
"""
from utilities import mle_models as models
from utilities import mle_utils as utils
import numpy as np


def create_probability_map_uniform(x, y):
    """
    Create a flat distribution by combing a flat ridge
    and a flat fan.

    Parameters
    ----------
    x : (M,N) array-like
        all x coordinates for which to compute an amplitude.
    y : (M,N) array-like
        all y coordinates for which to compute an amplitude.

    Raises
    ------
    ValueError
        x an y dimension mismatch.

    Returns
    -------
    ampl : (M,N) array-like
        the amplitude map for the banana distribution.

    """
    if len(x) != len(y):
        raise ValueError("x and y inputs do not match in length!")
    r, phi = utils.cart2pol(x, y)
    ridge = models.calc_uniform_probability_density(r, 0, np.inf)

    if np.any(ridge == 0):
        ridge[ridge == 0] = np.finfo(np.float32).eps
        print(
            "found 0-valued probabilities for ridge, truncating to smallest nonzero value!"
        )

    fan = models.calc_uniform_probability_density(phi, -np.pi, 2 * np.pi)
    print(f"sum ridge: {np.sum(ridge)}, sum fan: {np.sum(fan)}")

    ampl = ridge * fan
    return ampl


def create_probability_map_gauss_banana(
    x, y, mu_ang, kappa_ang, mu_dist, sigma_dist
):
    """
    Create a banana-shaped distribution by combining a gaussian ridge
    and a von Mises distribution.

    Parameters
    ----------
    x : (M,N) array-like
        all x coordinates for which to compute an amplitude.
    y : (M,N) array-like
        all y coordinates for which to compute an amplitude.
    mu_ang : scalar
        the expected value of the von-mises pdf.
    kappa_ang : scalar
        the concentration parameter (approx 1/std) of the von-mises pdf.
    mu_dist : scalar
        expected value of the gaussian pdf.
    sigma_dist : scalar
        standard deviation of the gaussian pdf.

    Returns
    -------
    ampl : (M,N) array-like
        the amplitude map for the banana distribution.

    """
    if len(x) != len(y):
        raise ValueError("x and y inputs do not match in length!")
    r, phi = utils.cart2pol(x, y)
    # gaussian ridge with radius mu
    ridge = models.calc_gaussian_probability_density(
        x=r, mu=mu_dist, sigma=sigma_dist
    )
    # von mises centred on given rot, with kappa set to approximate given std
    fan = models.calc_von_mises_probability_density(
        theta=phi, mu=mu_ang, kappa=kappa_ang
    )

    ampl = ridge * fan
    return ampl


def create_probability_map_gamma_banana(
    x, y, mu_ang, kappa_ang, alpha_dist, beta_dist
):
    """
    Create a banana-shaped distribution by combining a gamma
    and a von Mises distribution.

    Parameters
    ----------
    x : (M,N) array-like
        all x coordinates for which to compute an amplitude.
    y : (M,N) array-like
        all y coordinates for which to compute an amplitude.
    mu_ang : scalar
        the expected value of the von-mises pdf.
    kappa_ang : scalar
        the concentration parameter (approx 1/std) of the von-mises pdf.
    alpha_dist : scalar
        shape parameter of the gamma pdf.
    beta_dist : scalar
        inverse scale parameter of the gamma pdf.

    Returns
    -------
    ampl : (M,N) array-like
        the amplitude map for the banana distribution.
    """
    if len(x) != len(y):
        raise ValueError("x and y inputs do not match in length!")
    r, phi = utils.cart2pol(x, y)
    # gamma dsitribution along distance axis
    ridge = models.calc_gamma_probability_density(
        x=r, alpha=alpha_dist, beta=beta_dist
    )
    # von mises centred on given rot, with kappa set to approximate given std
    fan = models.calc_von_mises_probability_density(
        theta=phi, mu=mu_ang, kappa=kappa_ang
    )

    ampl = ridge * fan
    return ampl
