"""
:AUTHORS: Martin M. Müller
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: martin.mueller@uni-bielefeld.de
:SINCE: Mon Dec 13 15:34:40 2021
"""

import numpy as np
import pandas as pd

from utilities import mle_models as models
from utilities import mle_model_eval as mleval


def evaluate_gauss_banana(
    data_dist, data_ang, mu_vm, kappa, mu_gauss, sigma, model_name
):
    p_vm = models.calc_von_mises_probability_density(data_ang, mu_vm, kappa)
    p_gauss = models.calc_gaussian_probability_density(
        data_dist, mu_gauss, sigma
    )
    "calculate log-likelihoods"
    ll_vm = np.sum(np.log(p_vm))

    ll_gauss = np.sum(np.log(p_gauss))

    "create output dataframe and collect results"
    summary_df = pd.DataFrame(
        columns=["model", "log_likelihood", "parameters"], dtype=object
    )
    component_variants = ["vm_fit", "gaussian_fit"]
    component_lls = [ll_vm, ll_gauss]
    parameters = [
        (mu_vm, kappa),
        (mu_gauss, sigma),
    ]

    "populate output dataframe"
    for name, ll, params in zip(component_variants, component_lls, parameters):
        summary_df = summary_df.append(
            {
                "model": model_name + name,
                "log_likelihood": ll,
                "parameters": params,
            },
            ignore_index=True,
        )
    return summary_df


def evaluate_uniform(data_dist, data_ang, model_name):
    # handle distance data
    p_dist = models.calc_uniform_probability_density(data_dist, 0, np.inf)
    if np.any(p_dist == 0):
        print(
            "found 0-valued probabilities for distance data, truncating to smallest nonzero value!"
        )
        p_dist[p_dist == 0] = np.finfo(np.float32).eps
    # handle angle data
    p_ang = models.calc_uniform_probability_density(
        data_ang, -np.pi, 2 * np.pi
    )
    if np.any(p_ang == 0):
        print(
            "found 0-valued probabilities for angle data, truncating to smallest nonzero value!"
        )
        p_ang[p_ang == 0] = np.finfo(np.float32).eps

    "calculate log-likelihoods"
    ll_dist = np.sum(np.log(p_dist))
    ll_ang = np.sum(np.log(p_ang))

    "create output dataframe and collect results"
    summary_df = pd.DataFrame(
        columns=["model", "log_likelihood"], dtype=object
    )
    component_variants = ["ang_flat", "dist_flat"]
    component_lls = [ll_ang, ll_dist]

    "populate output dataframe"
    for name, ll in zip(component_variants, component_lls):
        summary_df = summary_df.append(
            {
                "model": model_name + name,
                "log_likelihood": ll,
            },
            ignore_index=True,
        )
    return summary_df


def evaluate_gamma_banana(
    data_dist, data_ang, mu_vm, kappa, alpha, beta, model_name
):
    p_vm = models.calc_von_mises_probability_density(data_ang, mu_vm, kappa)
    p_gamma = models.calc_gamma_probability_density(data_dist, alpha, beta)
    "calculate log-likelihoods"
    ll_vm = np.sum(np.log(p_vm))

    ll_gauss = np.sum(np.log(p_gamma))

    "create output dataframe and collect results"
    summary_df = pd.DataFrame(
        columns=["model", "log_likelihood", "parameters"], dtype=object
    )
    component_variants = ["vm_fit", "gamma_fit"]
    component_lls = [ll_vm, ll_gauss]
    parameters = [
        (mu_vm, kappa),
        (alpha, beta),
    ]

    "populate output dataframe"
    for name, ll, params in zip(component_variants, component_lls, parameters):
        summary_df = summary_df.append(
            {
                "model": model_name + name,
                "log_likelihood": ll,
                "parameters": params,
            },
            ignore_index=True,
        )
    return summary_df


def model_banana_variants(data_dist, data_ang, name_prefix="", mode="gamma"):
    """
    Fit models for distance and angle errors. Distance errors can be modelled
    using either a gamma or gaussian distribution,
    angle errors are modelled using a von mises distribution.

    Parameters
    ----------
    data_dist : (N,1) array-like
        distance error samples.
    data_ang : (N,1) array-like
        angle error samples.
    name_prefix : string, optional
        prefix for model names, to facilitate repeat calls to this function
        for different models, all saved to the same dataframe.
        The default is "".
    mode : string, optional
        whether to use a 'gamma' or 'gaussian' for the distance data.
        The default is "gamma".

    Raises
    ------
    ValueError
        raised if unknown mode is provided.

    Returns
    -------
    summary_df : pandas dataframe
        a dataframe with colums ["model", "log_likelihood", "parameters"].
        each row contains the name of the model,
        the resulting log-likelihood of the model for the given data,
        as well as the parameters used for the model fit
        (i.e. the MLE of the parameters)
    """

    if not (mode == "gamma" or mode == "gaussian"):
        raise ValueError(f"function has no mode '{mode}'!")
    "do mle for von mises on angle error"
    mu_vm_fit, kappa_fit = mleval.find_mle_von_mises(data_ang)
    mu_vm_dummy, kappa_dummy = models.von_mises_flat()

    p_vm_fit = models.calc_von_mises_probability_density(
        data_ang, mu_vm_fit, kappa_fit
    )
    p_vm_flat = models.calc_von_mises_probability_density(
        data_ang, mu_vm_dummy, kappa_dummy
    )

    "do mle for gamma or gauss on dist error"
    if mode == "gamma":
        # find mle for parameters of gamma function, given the data
        alpha_fit, beta_fit = mleval.find_mle_gamma(data_dist)
        params_dist_fit = (alpha_fit, beta_fit)
        # collect probabilities of the data, given the fitted model
        p_dist_fit = models.calc_gamma_probability_density(
            data_dist, alpha_fit, beta_fit
        )
        # do the same for flat dummy function
        alpha_dummy, beta_dummy = models.gamma_flat()
        params_dist_flat = (alpha_dummy, beta_dummy)

        p_dist_flat = models.calc_gamma_probability_density(
            data_dist, alpha_dummy, beta_dummy
        )
    if mode == "gaussian":
        # find mle for parameters of gaussian function, given the data
        mu_gauss_fit, sigma_fit = mleval.find_mle_gaussian(data_dist)
        params_dist_fit = (mu_gauss_fit, sigma_fit)
        # collect probabilities of the data, given the fitted model
        p_dist_fit = models.calc_gaussian_probability_density(
            data_dist, mu_gauss_fit, sigma_fit
        )
        # do the same for flat dummy function
        mu_gauss_dummy, sigma_dummy = models.gauss_flat()
        params_dist_flat = (mu_gauss_dummy, sigma_dummy)

        p_dist_flat = models.calc_gaussian_probability_density(
            data_dist, mu_gauss_dummy, sigma_dummy
        )
    "calculate log-likelihoods"
    ll_vm_flat = np.sum(np.log(p_vm_flat))
    ll_vm_fit = np.sum(np.log(p_vm_fit))

    ll_dist_flat = np.sum(np.log(p_dist_flat))
    ll_dist_fit = np.sum(np.log(p_dist_fit))

    "create output dataframe and collect results"
    summary_df = pd.DataFrame(
        columns=["model", "log_likelihood", "parameters"], dtype=object
    )
    component_variants = ["vm_fit", "vm_flat", f"{mode}_fit", f"{mode}_flat"]
    component_lls = [ll_vm_fit, ll_vm_flat, ll_dist_fit, ll_dist_flat]
    parameters = [
        (mu_vm_fit, kappa_fit),
        (mu_vm_dummy, kappa_dummy),
        params_dist_fit,
        params_dist_flat,
    ]

    "populate output dataframe"
    for name, ll, params in zip(component_variants, component_lls, parameters):
        summary_df = summary_df.append(
            {
                "model": name_prefix + name,
                "log_likelihood": ll,
                "parameters": params,
            },
            ignore_index=True,
        )
    return summary_df


def combine_models(summary_df, to_combine, verbose=False):
    """
    Calculate the overall log-likelihood of different models.


    Parameters
    ----------
    summary_df : pandas dataframe, as produced by 'model_banana_variants'
        a datframe containing models to be combined.
    to_combine : list of strings
        which models to combine.
    verbose : bool, optional
        whether to print results to console. The default is False.

    Raises
    ------
    ValueError
        raised if models named in to_combine are not present in summary_df.

    Returns
    -------
    ll_combined : scalar
        the log-likelihood of the combined model.
    name_string : string
        a string representation of the model combination.

    """
    ll_combined = 0
    for model in to_combine:
        if not model in summary_df["model"].values:
            raise ValueError(f"no model named '{model}' found in summary_df!")
        row = summary_df["model"] == model
        ll_combined += summary_df.loc[row, "log_likelihood"].values[0]
    name_string = " + ".join(to_combine)
    if verbose == True:
        print(
            f"model combination {name_string} had total log-likelihood of"
            + f" {ll_combined}."
        )
    return ll_combined, name_string
